import axios from 'axios';
import { Subject } from 'rxjs';

class Http {
  axios = null

  errorObserver = null

  baseUrl = null

  constructor() {
    this.errorObserver = new Subject();

    this.axios = axios.create();
    this.post = this.axios.post;
    this.get = this.axios.get;
    this.put = this.axios.put;
    this.delete = this.axios.delete;
    this.axios.interceptors.request.use((request) => request, this.errorInterceptor);
    this.axios.interceptors.response.use((response) => response, this.errorInterceptor);
  }

  errorInterceptor = (error) => {
    this.errorObserver.next(error);
    return Promise.reject(error);
  }

  subscribeApiError$() {
    return this.errorObserver.asObservable();
  }
}

export default Http;
