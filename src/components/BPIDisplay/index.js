import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Grid, Box, Typography, IconButton, TextField,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { fetchBpi } from 'modules/app';
import { getBpi } from 'modules/app/selector';
import CircularProgress from '@material-ui/core/CircularProgress';
import CurrencyTextField from '@unicef/material-ui-currency-textfield';
import SelectCurrencies from './selectCurrencies';
import styles from './style';

class BPIDisplay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleCurrencies: ['USD'],
      btc: 1,
    };
  }

  componentDidMount() {
    const { fetchBpiFunc } = this.props;
    fetchBpiFunc();
    this.fetchInterval = setInterval(() => {
      fetchBpiFunc();
    }, 60 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.fetchInterval);
  }

  setBTCamount(event) {
    let btcValue = event.target.value.toString();
    btcValue = btcValue.replace(/[^0-9,.]/, '').replace(/,/, '.');
    const regexp = RegExp(/(\d{0,16}\.?\d{0,16})?/, 'g');
    const isThisANumber = btcValue.match(regexp);
    if (typeof isThisANumber?.length !== 'undefined' && isThisANumber?.length < 3) {
      this.setState({ btc: btcValue });
    }
  }

  removeCurrency(currency) {
    const { visibleCurrencies } = this.state;
    const currencies = visibleCurrencies.filter((item) => item !== currency);
    this.setState({ visibleCurrencies: currencies });
  }

  render() {
    const { bpi, classes } = this.props;
    const { visibleCurrencies, btc } = this.state;
    const btcNumber = Number(btc);
    let content = (
      <Grid container alignItems="center" justify="center" className={classes.loaderBox}>
        <Box>
          <CircularProgress color="primary" />
        </Box>
      </Grid>
    );
    if (bpi.size > 0) {
      content = (
        <Grid>
          <Box>
            <Box mb={2}>
              <Typography>
                Step 1. Add amount of BTC
              </Typography>
            </Box>
            <Box mb={4}>
              <TextField
                label="Amount of BTC"
                variant="outlined"
                value={btc}
                fullWidth
                onChange={(event) => this.setBTCamount(event)}
              />
            </Box>
            <Box mb={2}>
              <Typography>
                Step 2. Select currencies
              </Typography>
            </Box>
            <Box mb={2}>
              <SelectCurrencies setVisibleCurrencies={(value) => this.setState({ visibleCurrencies: value })} visibleCurrencies={visibleCurrencies} bpi={bpi} />
            </Box>
            <Box className={classes.currenciesBlock}>
              {visibleCurrencies.includes('USD') && (
              <Box pb={2}>
                <Grid container>
                  <CurrencyTextField
                    label={bpi.getIn(['USD', 'description'])}
                    variant="outlined"
                    value={btcNumber * bpi.getIn(['USD', 'rate_float'])}
                    currencySymbol="$"
                    minimumValue="0"
                    outputFormat="string"
                    decimalCharacter="."
                    digitGroupSeparator=","
                    readOnly
                  />
                  <IconButton onClick={() => this.removeCurrency('USD')}>
                    <Close />
                  </IconButton>
                </Grid>
              </Box>
              )}
              {visibleCurrencies.includes('EUR') && (
              <Box pb={2}>
                <Grid container>
                  <CurrencyTextField
                    label={bpi.getIn(['EUR', 'description'])}
                    variant="outlined"
                    value={btcNumber * bpi.getIn(['EUR', 'rate_float'])}
                    currencySymbol="€"
                    minimumValue="0"
                    outputFormat="string"
                    decimalCharacter="."
                    digitGroupSeparator=","
                    readOnly
                  />
                  <IconButton onClick={() => this.removeCurrency('EUR')}>
                    <Close />
                  </IconButton>
                </Grid>
              </Box>
              )}
              {visibleCurrencies.includes('GBP') && (
              <Box pb={2}>
                <Grid container>
                  <CurrencyTextField
                    label={bpi.getIn(['GBP', 'description'])}
                    variant="outlined"
                    value={btcNumber * bpi.getIn(['GBP', 'rate_float'])}
                    currencySymbol="£"
                    minimumValue="0"
                    outputFormat="string"
                    decimalCharacter="."
                    digitGroupSeparator=","
                    readOnly
                  />
                  <IconButton onClick={() => this.removeCurrency('GBP')}>
                    <Close />
                  </IconButton>
                </Grid>
              </Box>
              )}
            </Box>
          </Box>
        </Grid>
      );
    }

    return (
      <Box py={2} px={2}>
        <Box px={4} py={4} className={classes.mainContainer}>
          <Box mb={4}>
            <Typography variant="h4" component="h1" className={classes.mainTitle}>
              BTC price converter
            </Typography>
          </Box>
          {content}
        </Box>
      </Box>

    );
  }
}

BPIDisplay.propTypes = {
  fetchBpiFunc: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  bpi: PropTypes.object,
};

BPIDisplay.defaultProps = {
  bpi: {},
};

const mapStateToProps = (state) => ({
  bpi: getBpi(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchBpiFunc: () => dispatch(fetchBpi()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BPIDisplay));
