/* eslint-disable import/prefer-default-export */
export const getBpi = (state) => state.app.bpi;
export const getAlerts = (state) => state.app.alerts;
