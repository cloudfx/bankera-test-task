import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';

import createRootReducer from './rootReducer';
import rootEpic from './rootEpics';

const composeEnhancers = process.env.NODE_ENV === 'development' ? composeWithDevTools({}) : () => {};
const epicMiddleware = createEpicMiddleware();

export default function configureStore(preloadedState) {
  const store = createStore(
    createRootReducer(),
    preloadedState,
    composeEnhancers(applyMiddleware(epicMiddleware)),
  );

  epicMiddleware.run(rootEpic);

  return store;
}
