import theme from 'theme';

export default {
  notificationsWrapper: {
    position: 'fixed',
    bottom: 0,
    left: 0,
  },
  notification: {
    padding: '8px 16px',
    maxWidth: 350,
    width: '100%',
    background: theme.colors.Error,
    color: theme.colors.White,
    borderRadius: 6,
    boxShadow: '0 0 3px 3px rgba(0,0,0,0.1)',
  },
};
