import React, { useState, useEffect } from 'react';

import { Provider } from 'react-redux';
import { ThemeProvider, Box } from '@material-ui/core';
import theme from 'theme';
import { BPIDisplay, NotificationsDisplay } from 'components';
import { startApp } from 'modules/app';
import BackgroundImage from 'assets/images/cloud.svg';
import configureStore from './modules';

const store = configureStore(/* provide initial state if any */);

const styles = {
  paperContainer: {
    minHeight: '100vh',
    backgroundImage: `url(${BackgroundImage})`,
    backgroundRepeat: 'no-repeat',
  },
};

export default () => {
  const [isAppReady, setIsAppReady] = useState(false);

  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      if (store.getState().app.isReady !== isAppReady) {
        setIsAppReady(store.getState().app.isReady);
      }
    });
    return unsubscribe;
  }, [isAppReady]);

  useEffect(() => {
    store.dispatch(startApp());
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Box style={styles.paperContainer}>
          <BPIDisplay />
          <NotificationsDisplay />
        </Box>
      </Provider>
    </ThemeProvider>
  );
};
