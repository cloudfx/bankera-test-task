import { handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import moment from 'moment';
import {
  appStarted, updateBpi, addAlert, removeAlert,
} from './actions';

const initialState = {
  alerts: Map(),
  alertsIds: List(),
  isReady: false,
  bpi: Map({}),
  executionId: '',
};

export default handleActions(
  {
    [appStarted]: (state) => ({
      ...state,
      isReady: true,
    }),
    [addAlert]: (state, { payload }) => ({
      ...state,
      alerts: state.alerts.set(payload.get('id'), payload),
      alertsIds: state.alertsIds.push(payload.get('id')),
    }),
    [removeAlert]: (state, { payload }) => ({
      ...state,
      alerts: state.alerts.delete(payload),
      alertsIds: state.alertsIds.filterNot((id) => id === payload),
    }),
    [updateBpi]: (state, { payload }) => ({
      ...state,
      updated: moment(),
      bpi: Map(payload),
    }),
  },
  initialState,
);
