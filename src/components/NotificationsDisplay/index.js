import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Box, Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { getAlerts } from 'modules/app/selector';
import styles from './style';

class NotificationsDisplay extends PureComponent {
  render() {
    const { notifications, classes } = this.props;
    return (
      <Box className={classes.notificationsWrapper}>
        {Object.keys(notifications).length > 0 && (
        <Box py={4} px={4}>
          {Object.keys(notifications).map((id) => (
            <React.Fragment key={id}>
              <Box py={4} px={4} className={classes.notification}>
                <Typography>
                  {notifications[id].description}
                </Typography>
              </Box>
            </React.Fragment>
          ))}
        </Box>
        )}
      </Box>

    );
  }
}

NotificationsDisplay.propTypes = {
  classes: PropTypes.object.isRequired,
  notifications: PropTypes.object,
};

NotificationsDisplay.defaultProps = {
  notifications: {},
};

const mapStateToProps = (state) => ({
  notifications: getAlerts(state).toJS(),
});

export default connect(mapStateToProps, null)(withStyles(styles)(NotificationsDisplay));
