export default {
  mainContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100%',
    maxWidth: 560,
    background: '#fff',
    borderRadius: 8,
  },
  loaderBox: {
    minHeight: 300,
  },
  mainTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  formControl: {
    width: '100%',
  },
  btcInput: {
    '& input': {
      textAlign: 'right',
      paddingRight: 22,
    },
  },
  currenciesBlock: {
    '& .MuiFormControl-root': {
      minWidth: 'calc(100% - 50px)',
    },
  },
};
