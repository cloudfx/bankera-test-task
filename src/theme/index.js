import { createMuiTheme } from '@material-ui/core';

import colors from './colors';

export default createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      w1100: 1100,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: colors.Primary1,
    },
    secondary: {
      main: colors.Grey70,
    },
    background: {
      default: colors.Grey4,
    },
    error: {
      main: colors.Error,
    },
  },
  typography: {
    fontFamily: ['"Roboto"', 'Arial', 'sans-serif'].join(', '),
  },
  btn: {
    backgroundImage: 'none',
    outline: 0,
    border: 'none',
    borderRadius: 'none',
    boxShadow: 'none',
    color: colors.White,
    fontWeight: 300,
    fontSize: '14px',
    transition: 'all .5s ease-in-out',
    cursor: 'pointer',
    '&:disabled': {
      background: colors.Grey40,
    },
    '&:grey': {
      background: colors.Grey82,
      color: colors.White,
    },
  },
  colors,
});
