import { combineEpics, ofType } from 'redux-observable';
import {
  switchMap, delay, mapTo, mergeMap, map, catchError,
} from 'rxjs/operators';
import { empty, from, of } from 'rxjs';
import { Map } from 'immutable';
import Api, { Http } from 'api';
import { v4 as uuid } from 'uuid';
import { Alert, AlertStatus } from 'types';
import {
  startApp, appStarted, addAlert, removeAlert, fetchBpi, updateBpi,
} from './actions';

const initEpic = (action$) => action$.pipe(
  ofType(startApp.toString()),
  switchMap(() => {
    const actions = [appStarted()];
    return of(...actions);
  }),
);

const subscribeHttpErrorsEpic = (action$) => action$.pipe(
  ofType(startApp.toString()),
  switchMap(() => Http.subscribeApiError$().pipe(
    switchMap(({ message }) => {
      console.log({ message });
      const alertId = uuid();
      return of(addAlert(Map(new Alert({ id: alertId, description: message, status: AlertStatus.ERROR }))));
    }),
  )),
);

const autoRemoveAlertEpic = (action$) => action$.pipe(
  ofType(addAlert.toString()),
  mergeMap((action) => of(action).pipe(delay(10000), mapTo(removeAlert(action.payload.get('id'))))),
);

const fetchBpiEpic = (action$) => action$.pipe(
  ofType(fetchBpi.toString()),
  switchMap(() => from(Api.priceIndex.fetchBpi()).pipe(
    map((data) => {
      if (data && data.data) {
        return data.data.bpi;
      }
      throw new Error('No BPI data provided');
    }),
    switchMap((data) => of(updateBpi(data))),
    catchError((err) => {
      console.log(err);
      return empty();
    }),
  )),
);

export default combineEpics(
  initEpic,
  subscribeHttpErrorsEpic,
  autoRemoveAlertEpic,
  fetchBpiEpic,
);
