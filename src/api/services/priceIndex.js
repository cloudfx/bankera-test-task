import Client from './client';

class PriceIndex extends Client {
  fetchBpi = () => this.http.get('https://api.coindesk.com/v1/bpi/currentprice.json');
}

export default PriceIndex;
