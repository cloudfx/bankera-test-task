import { v4 as uuid } from 'uuid';

export const AlertStatus = {
  ERROR: 'error',
  WARNING: 'warning',
  INFO: 'info',
  SUCCESS: 'success',
};

export class Alert {
  constructor({
    id = uuid(),
    title = null,
    description = 'Some error occured...',
    status = AlertStatus.INFO,
  } = {}) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.status = status;
  }
}
