import { createAction } from 'redux-actions';

export const APP_START = 'app/START';
export const APP_STARTED = 'app/STARTED';

export const ADD_ALERT = 'app/ADD_ALERT';
export const REMOVE_ALERT = 'app/REMOVE_ALERT';

export const FETCH_BPI = 'app/FETCH_BPI';
export const UPDATE_BPI = 'app/UPDATE_BPI';

export const startApp = createAction(APP_START);
export const appStarted = createAction(APP_STARTED);
export const addAlert = createAction(ADD_ALERT);
export const removeAlert = createAction(REMOVE_ALERT);
export const fetchBpi = createAction(FETCH_BPI);
export const updateBpi = createAction(UPDATE_BPI);
