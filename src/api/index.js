import Http from './http';
import { PriceIndex } from './services';

const http = new Http();
export { http as Http };

const Api = {
  priceIndex: new PriceIndex(http),
};
export default Api;
