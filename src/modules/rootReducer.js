import { combineReducers } from 'redux';
import { reducer as app } from './app';

const createRootReducer = () => combineReducers({
  app,
});

export default createRootReducer;
