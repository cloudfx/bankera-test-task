import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  formControl: {
    width: '100%',
  },
  chip: {

  },
}));

const SelectCurrencies = ({ visibleCurrencies, setVisibleCurrencies, bpi }) => {
  const classes = useStyles();

  const handleChange = (event) => {
    setVisibleCurrencies(event.target.value);
  };

  return (
    <Box width="100%">
      <FormControl className={classes.formControl}>
        <InputLabel>Select currencies for convertion</InputLabel>
        <Select
          multiple
          displayEmpty
          value={visibleCurrencies}
          onChange={handleChange}
          renderValue={(selected) => (
            <div className={classes.chips}>
              {selected.map((value) => (
                <Chip key={value} label={value} className={classes.chip} />
              ))}
            </div>
          )}
        >
          {bpi.map((item) => (
            <MenuItem key={item.code} value={item.code}>
              {item.description}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

SelectCurrencies.propTypes = {
  setVisibleCurrencies: PropTypes.func.isRequired,
  visibleCurrencies: PropTypes.array,
  bpi: PropTypes.object.isRequired,
};

SelectCurrencies.defaultProps = {
  visibleCurrencies: [],
};

export default SelectCurrencies;
